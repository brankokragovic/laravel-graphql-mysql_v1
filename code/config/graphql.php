<?php


return [

    // The prefix for routes
    'prefix' => 'graphql',


    'routes' => '{graphql_schema?}',


    'controllers' => \Folklore\GraphQL\GraphQLController::class.'@query',
    

    'variables_input_name' => 'params',


    'middleware' => [],

    'graphiql' => [
        'routes' => '/graphiql',
        'middleware' => [],
        'view' => 'graphql::graphiql'
    ],


    'schema' => 'default',

    'schemas' => [
        'default' => [
            'query' => [
                'users' => 'App\GraphQL\Query\UsersQuery'
            ],
            'mutation' => [
                'updateUserPassword' => 'App\GraphQL\Mutation\UpdateUserPasswordMutation',
                'updateUserEmail' => 'App\GraphQL\Mutation\UpdateUserEmailMutation',

            ]
        ],
        'secret' => [
            'query' => [
                'users' => 'App\GraphQL\Query\UsersQuery'
            ],

        ]
    ],

    'types' => [
        'User' => 'App\GraphQL\Type\UserType'
    ],

    // This callable will received every Error objects for each errors GraphQL catch.
    // The method should return an array representing the error.
    //
    // Typically:
    // [
    //     'message' => '',
    //     'locations' => []
    // ]
    //
    'error_formatter' => [\Folklore\GraphQL\GraphQL::class, 'formatError'],

    // Options to limit the query complexity and depth. See the doc
    // @ https://github.com/webonyx/graphql-php#security
    // for details. Disabled by default.
    'security' => [
        'query_max_complexity' => null,
        'query_max_depth' => null
    ]
];
